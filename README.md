# laraminio
Example using minio as Laravel cloud storage

## How to ?
1. Configure `.env`, set some Minio config such as (`S3_ENDPOINT`, `S3_KEY`, `S3_SECRET`, `S3_BUCKET`)
2. Configure s3 config in `config/filesystems.php` make it read config from `.env`
3. Ready to use!


## Example store uploaded files
```php
// Get file extension
$ext = $request->file('image')->getClientOriginalExtension();

// Set filename
$filename = sprintf("image_%s.%s", strtoupper(str_random(5)), $ext);

// Store to disk
$result = Storage::disk('s3')->putFile('photos', $request->file('image'), $filename);

// Get url, make sure set permission to public 
$url = Storage::disk('s3')->url($result);
```
